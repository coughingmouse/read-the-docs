# Configuration file for the Sphinx documentation builder.

# -- Project information

project = 'Mawusu'
copyright = '2022-2024, Iso Lee'
author = 'Coughingmouse'

release = '0.1'
version = '0.1.3'

# -- General configuration

extensions = [
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.intersphinx',
]

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

# -- Options for HTML output

html_theme = 'furo'

# -- Options for EPUB output

epub_show_urls = 'footnote'

html_static_path = ['_static']

html_title = ""

html_theme_options = {
    "light_logo": "cough_trans.png",
    "dark_logo": "cough_trans_invt.png"
}

# -- Adds Stylesheet

html_css_files = [
    'styles.css',
]

# -- Adds support for more languages on PDF

latex_engine = "xelatex"
latex_use_xindy = False
latex_elements = {
    'fontpkg': r"""
\usepackage[abspath]{currfile}
\usepackage{noto}
\getpwd
% \setmonofont{\thepwd../../docs/source/_static/Fira_Code/static/FiraCode-Regular.ttf}
% \usepackage{dhucs-nanumfont}
""",
    "preamble": r"""
\usepackage[AutoFallBack=true, CJKspace]{xeCJK}
\setCJKmainfont{Noto Serif CJK KR}
\setCJKsansfont{Noto Sans CJK KR}
\setCJKmonofont{Noto Sans CJK KR}
\setCJKfallbackfamilyfont{\CJKrmdefault}[AutoFakeBold]{{HanaMinA},{HanaMinB}}
\setCJKfallbackfamilyfont{\CJKsfdefault}[AutoFakeBold]{{HanaMinA},{HanaMinB}}
\setCJKfallbackfamilyfont{\CJKttdefault}[AutoFakeBold]{{HanaMinA},{HanaMinB}}

\AtBeginDocument{%
  \XeTeXcharclass`^^^^2026=0
  \XeTeXcharclass`^^^^2019=0
  \XeTeXcharclass`^^^^201a=0
  \XeTeXcharclass`^^^^201c=0
  \XeTeXcharclass`^^^^201d=0
}
\XeTeXinterchartokenstate=1
\newXeTeXintercharclass\laclass

\XeTeXcharclass `\⟨ \laclass

\XeTeXinterchartoks 0 \laclass = {\myla}
\XeTeXinterchartoks 4095 \laclass = {\myla}
\XeTeXinterchartoks \laclass \laclass = {\myla}

\newcommand\myla[1]{\textsf{$\langle$}}

\newXeTeXintercharclass\raclass

\XeTeXcharclass `\⟩ \raclass

\XeTeXinterchartoks 0 \raclass = {\myra}
\XeTeXinterchartoks 4095 \raclass = {\myra}
\XeTeXinterchartoks \raclass \raclass = {\myra}

\newcommand\myra[1]{\textsf{$\rangle$}}

\newXeTeXintercharclass\vnclass

\XeTeXcharclass `\∅ \vnclass

\XeTeXinterchartoks 0 \vnclass = {\myvn}
\XeTeXinterchartoks 4095 \vnclass = {\myvn}
\XeTeXinterchartoks \vnclass \vnclass = {\myvn}

\newcommand\myvn[1]{\textsf{$\varnothing$}}

\newXeTeXintercharclass\eclass

\XeTeXcharclass `\∃ \eclass

\XeTeXinterchartoks 0 \eclass = {\mye}
\XeTeXinterchartoks 4095 \eclass = {\mye}
\XeTeXinterchartoks \eclass \eclass = {\mye}

\newcommand\mye[1]{\textsf{$\exists$}}

"""
}

# \usepackage[UTF8]{ctex}
# \usepackage[AutoFallBack=true]{xeCJK}
# \setCJKfallbackfamilyfont{\CJKrmdefault}[AutoFakeBold]{{HanaMinA},{HanaMinB}}
# \setCJKfallbackfamilyfont{\CJKsfdefault}[AutoFakeBold]{{HanaMinA},{HanaMinB}}
# \setCJKfallbackfamilyfont{\CJKttdefault}[AutoFakeBold]{{HanaMinA},{HanaMinB}}
# \usepackage{graphicx}
# \graphicspath{ {/korean/} }
# \usepackage{lscape}
# \setCJKmainfont{Noto Serif CJK SC}[Language=Chinese Simplified, BoldFont={* Bold}, ItalicFont=AR PL KaitiM GB]
# \setCJKsansfont{Noto Sans CJK SC}[Language=Chinese Simplified, BoldFont={* Bold}, ItalicFont=AR PL KaitiM GB]
# \setCJKmonofont{Noto Sans CJK SC}[Language=Chinese Simplified, BoldFont={* Bold}, ItalicFont=AR PL KaitiM GB]
# \defaultfontfeatures{Ligatures={NoCommon, NoDiscretionary, NoHistoric, NoRequired, NoContextual}}
# \setmainfont{unifont.otf}
# \setsansfont{unifont.otf}
# \setmonofont{unifont.otf}
# \usepackage{fontspec}


