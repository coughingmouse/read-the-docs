Mow5su5 Literary Speech Alphabet
================================

Gu & Simmons' system, modified slightly to be written using only Basic
Latin letters. Made to be regular, easy, and compatible with Common
Speech.

== == == === === === ==== ===
bh dh qh gh  xh  dyh ddh  drh
p  t  c  k   h   ty  tt   tr
b  d  q  g       dy  dd   dr
mh nh lh ngh yh           rh
m  n  l  ng  y            r
mv
f     s              ss   sr
v     z              (zz) zr
== == == === === === ==== ===

=== === ==== ======== ====== === ======== ========
o       wo            a          ea       
wa                    e          ie       je
w   j                 ai, oi eai wai, woi 
ei  wei               i      wi           
ow  uw  iuw           ew     eaw iaw      
om  op  am   ap       eam    eap iem      iep
um  up  im   ip       on     ot  won      wot
an  at                                    
ean eat wan  wat      ien    iet jam      jet
un  ut  wn   wt       in     it  jn       jt
ang ok  eang eok      wang   wok iang     iok
eng ek  weng uak, wek jeng       ieng     iek, jak
ung uk  wung wuk      ing    ik           jik
wng wk  ong  owk      jng    jk  iong     iowk
=== === ==== ======== ====== === ======== ========

-  Add coda r according to Common Speech.
-  Note that ?r and ?h are switched.

== == == ========
A  B  C  D
== == == ========
aa ah ax ap/at/ak
== == == ========

-  Also, you can use tone numbers 0-8. Use tone numbers
   [0, 1, 2, 3, 5] according to Common Speech tone for 入声.
