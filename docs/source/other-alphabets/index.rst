Other Alphabets
===============

.. toctree::
    :maxdepth: 1

    common-speech
    literary-speech
    general-american
    english
