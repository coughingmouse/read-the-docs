Maw5si5 Common Speech Alphabet
==============================

Yet Another Standard Chinese Romanization

Description
-----------

There's bopomofo which makes great use of the Chinese phonology, but
there's no equivalent chart for Pinyin! So here I give a demonstration of a
system that should do just that by remapping Pinyin.

That being said, Pinyin is great!
Let's make Pinyin more commonplace!

I just hope that there are some modifications.
We could 

#. officially allow using ⟨v⟩ instead of ⟨ü⟩
#. use ⟨r⟩ after ⟨z⟩/⟨c⟩/⟨s⟩ instead of ⟨h⟩ like in Tibetan
#. allow ⟨q⟩ to be used instead of ⟨'⟩ when not possible
#. write rising tone with ⟨5⟩ not ⟨4⟩, so that it can extend to writing for non-standard tones
#. use diacritics on the first vowel character. That is, on i/u/ü that stands for ㄧ/ㄨ/ㄩ primarily, rather than on a/e/o that stands for ㄚ/ㄛ/ㄜ primarily.

Demonstration
-------------

== === ========== ========== ==========
\  ∅   j          w          y
== === ========== ========== ==========
∅  ē   yi/-i      wo/-uo     yu/-ü
∅  á   ya/-ia     wa/-ua     
i  ěi  ye/-ie     wei/-ui    yue/-üe
i  ài  /yai/-iai  wai/-uai   
o  ·ou you/-iu    wu/-u      
o  ao  yao/-iao   /-o        /-io
n  en  yin/-in    wen/-un    yun/-ün
n  an  yan/-ian   wan/-uan   yuan/-üan
ng eng ying/-ing  weng/-ong  yong/-iong
ng ang yang/-iang wang/-uang 
== === ========== ========== ==========

Example Romanization
--------------------

==== ==== ==== ====
e    i    o    u
a    ia   oa
ëy   ië   oëy  uë
ay   /iay oay
ew   iw   ow
aw   iaw  /w   /uw
en   in   on   un
an   iän  oan  uän
eng  ing  ong  ung
ang  iang oang
==== ==== ==== ====

⟨h⟩ used instead of ⟨'⟩ to distinguish syllables in front of vowels.

-  Tones are marked with diacritics like Pinyin. 
   Alternatively, using ⟨1⟩ for īnping, ⟨2⟩ for iángping, ⟨3⟩
   for srǎng, ⟨5⟩ for qù, and ⟨0⟩ for qīng is allowed.
-  Every part of Standard Mandarin syllable should be present,
   especially the tonal markings.
-  Diacritics can usually be omitted while ⟨ae⟩ instead of ⟨ä⟩ can be
   used.
-  The system can be altered to remove ⟨n⟩ before ⟨g⟩.

Other phonemes are deriven from Pinyin but there are these following exceptions:

==== ====
else 
==== ====
?ê?  ?ä?
er   r
ar   ar
e-er er
e'er eer
yai  yai
?o   ?w
?io  ?uw
sh?  sr?
ch?  cr?
zh?  zr?
ji?  gi?
ju?  gu?
k?   q?
h?   x?
==== ====

For comparison,

== ====== ===== ==== ====
\  ∅      j     w    y
== ====== ===== ==== ====
∅  ㄜ     ㄭ/ㄧ ㄨㄛ ㄩ  
∅  ㄚˊ    ㄧㄚ  ㄨㄚ 
i  ㄟˇ    ㄧㄝ  ㄨㄟ ㄩㄝ
i  ㄞˋ    ㄧㄞ  ㄨㄞ 
o  ㄡˉ    ㄧㄡ  ㄨ
o  ㄠ     ㄧㄠ  ㄛ   ㄧㄛ 
n  ㄣ     ㄧㄣ  ㄨㄣ ㄩㄣ
n  ㄢ     ㄧㄢ  ㄨㄢ ㄩㄢ
ng ㄥ     ㄧㄥ  ㄨㄥ ㄩㄥ
ng ㄤ     ㄧㄤ  ㄨㄤ 
== ====== ===== ==== ====

--------------

Characteristics
---------------

-  phonemically more relevant and consistent, possibly providing better
   learning experience for both L1 and L2 users
-  apostrophe not used within core system to let for other uses
-  hyphen too
-  if we exclude Pinyin and its derivatives, this system is actually
   more in line with existing uses of the letters

-  mapping /k/ to ⟨q⟩ (and /-ng/ to ⟨g⟩) is unusual in the context of
   transcription of the sort
-  using ⟨x⟩ not as an alphabet is new

Design Principles
-----------------

-  Use bopomofo-based two vowel analysis
-  Be readable
-  Make sense
-  Be usable
-  Be typable with Qwerty keyboard for usability
-  Expanded letter should be typable with US international keyboard
-  Be compatible for passport names
-  Be compatible with Iso's Korean romanization.
-  Tones are not written using alphabet letters by historic restoration
   because of brevity

Roadmap
-------

-  Deploy

Contributing
------------

Whomever wishes to contribute may.

Authors and acknowledgment
--------------------------

Iso Lee

License
-------

Apache License 2.0

Project status
--------------

Not very active
