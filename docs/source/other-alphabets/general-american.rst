General American
================

This could be used to denote pronunciations.

Nucleus
-------

== ==== ==== ==== ==== ==== ====
\  a    i    u    e    o    u/i 
== ==== ==== ==== ==== ==== ====
w  a    i    u    e    o/ou au  
y  ae   ie   ue   ei   oi   ai  
wr ar   ir   ur   er   or   aur 
yr                     oir  air 
∃       -l   -r   -n   -m   eu  
== ==== ==== ==== ==== ==== ====

* Write ⟨-i⟩ as ⟨-y⟩ and ⟨-u⟩ as ⟨-w⟩ at the end of a word

Comparison to Merriam-Webster's
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

== ==== ==== ==== ==== ===== ====
\  a    i    u    e    o     u/i 
== ==== ==== ==== ==== ===== ====
w  ə    i    u̇    e    ō/ä   au̇  
y  a    ē    ü    ā    ȯi    ī   
wr är   ir   u̇r   er   ȯr    au̇r 
yr                     ȯi-ər ī-ər
∃       -l   -r   -n   -m    (œ) 
== ==== ==== ==== ==== ===== ====

* ⟨œ⟩ and ⟨ᵫ⟩ are only used for completely foreign words.

Comparison to English IPA (Wikipedia)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

== ==== ==== ==== ==== ========= ====
\  a    i    u    e    o         u/i 
== ==== ==== ==== ==== ========= ====
w  ʌ(ə) ɪ    ʊ    ɛ    oʊ/ɔː(ɑː) aʊ  
y  æ    iː   uː   eɪ   ɔɪ        aɪ  
wr ɑːr  ɪər  ʊər  ɛər  ɔər       aʊər
yr                     ɔɪər      aɪər
∃       -l   -r   -n   -m        ɜː  
== ==== ==== ==== ==== ========= ====

* ⟨ɜːr⟩, ⟨ær⟩, ⟨ʊr⟩, ⟨ɪr⟩, ⟨ɔːr⟩, ⟨ɑr⟩, ⟨ʌr⟩, and ⟨ɛr⟩ are not distinctive in General American.

Examples
~~~~~~~~

== ========== ===== ==== ===== ============= ====
\  a          i     u    e     o             u/i 
== ========== ===== ==== ===== ============= ====
w  what(bus)  lip   put  lemon so/tough(etc) bout
y  apple      leap  sue  late  boy           hi  
wr are        ear   poor air   or            our 
yr                             lawyer        lier
∃             camel her  Alan  rhythm        ö   
== ========== ===== ==== ===== ============= ====

Else
-------
b, g, d, f, z, zh, h, th, j, k, kh, l, m, n, x, p, ch, qu, r, s, sh, t, v, w, y

Contributing
------------

Whomever wishes to contribute may.

Authors and acknowledgment
--------------------------

Iso Lee

License
-------

Apache License 2.0

Project status
--------------

Not very active
