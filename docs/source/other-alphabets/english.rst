=======
English
=======

A generative approach to English vowels and a complaint on the ordering of letters.

Vowel
-----

====== ======= === ==
Vowel          Weak  
-------------- ------
Medial Nucleus IPA GV
====== ======= === ==
a      a       ə   a 
∅      i       ɪ   i 
u      u       u   u 
e      e       i   e 
o      h       ∅   ∅ 
====== ======= === ==

=== == ==== === ======= ======== ========================
Weak   -Ǝ       Comparison with General American English
------ -------- -----------------------------------------
IPA GV IPA  GV  Case    MGE|MW   Example
=== == ==== === ======= ======== ========================
∅   ∅  əl   -l  
uə  ua ər   -r  ue      /ua|ü-ə/ influence
iə  ea ən   -n  ia      /ia|ē-ə/ media
oʊ  oa əm   -m  o       /oa|ō/   retro
=== == ==== === ======= ======== ========================

=== == ==== === ======= ======= ========================
Strong -r       Comparison with General American English
------ -------- ----------------------------------------
IPA GV IPA  GV  Case    MGE|MW  Example
=== == ==== === ======= ======= ========================
aɪ  ai aɪər air ie      /ai|ī/  pie
ɪ   ui ɪr   uir i       /i|i/   pit
eɪ  ei ɛər  eir ei      /ei|ā/  eight
ɔɪ  oi ɔɪər oir oi      /oi|ȯi/ void
=== == ==== === ======= ======= ========================

=== == ==== === ======= ======= ========================
Strong -r       Comparison with General American English
------ -------- ----------------------------------------
IPA GV IPA  GV  Case    MGE|MW  Example
=== == ==== === ======= ======= ========================
aʊ  au aʊər aur ou      /au|au̇/ loud
ʊ   uu ʊr   uur u       /u|u̇/   put
ɜː  eu ɜːr  eur ö       /eu|œ/  Möbius (loanwords)
ɔː  ou ɔːr  our (o)/ou  /ou|ä/  (cot)/thought
=== == ==== === ======= ======= ========================

=== == ==== === ======= ======= ========================
Strong -r       Comparison with General American English
------ -------- ----------------------------------------
IPA GV IPA  GV  Case    MGE|MW  Example
=== == ==== === ======= ======= ========================
æ   ae ær   aer ae      /ae|a/  apple
uː  ue ʊər  uer ue      /ue|ü/  clue
iː  ee ɪər  eer ee      /ie|ē/  peek
oʊ  oe ɔər  oer oe      /o|ō/   toe, so
=== == ==== === ======= ======= ========================

=== == ==== === ======= ======= ========================
Strong -r       Comparison with General American English
------ -------- ----------------------------------------
IPA GV IPA  GV  Case    MGE|MW  Example
=== == ==== === ======= ======= ========================
ɑː  ah ɑːr  ahr a/(o)   /ou|ä/  father/(bother)
ʌ   uh ʌr   uhr u       /a|ə/   but/what
ɛ   eh ɛr   ehr e       /e|e/   met
ɒ   oh ɒr   ohr o       /ou|ä/  bother/cot
=== == ==== === ======= ======= ========================

Letter Order
------------

Alphabetic order
~~~~~~~~~~~~~~~~

1. We could switch ⟨G⟩, ⟨Z⟩, and ⟨C⟩. And swap ⟨X⟩ and ⟨Y⟩.
i.e. ABGDEFZ HIJK LMNOP QRS TUV WYXC

2. We could also relocate ⟨X⟩ and ⟨C⟩ where samekh and tsade belong.
i.e. ABGDEFZ HIJKLMN XOPCQRS TUVWY

3. We could make good use of @ as a teth (or θήτα) alternative.
We could always fall back to ⟨th⟩.
Looks close enough to a wheel, doesn't it?
In the alphabetic order, it would fit nicely after ⟨H⟩.
i.e. ABGDEFZ H\@JKLMN XOPCQRS TUVWYI

;Sik.saa order
~~~~~~~~~~~~~~

====== =========
Letter Name
====== =========
a      ei
i      ai
u      yue
------ ---------
e      hee(/ee/)
o      oe
h      heich
------ ---------
k      kei
q      kyue
g      gee
====== =========

====== =========
Letter Name
====== =========
x      eks
c      see
l      ehl
------ ---------
(@)    (thee)
j      jei
r      ahr
------ ---------
t      tee
d      dee
n      ehn
====== =========

====== =========
Letter Name
====== =========
p      pee
b      bee
m      ehm
------ ---------
f      ehf
v      vee
w      duhbl-yue
------ ---------
s      ehs
z      zee
y      wai
====== =========

