######################
Mawusu Korean Alphabet
######################

Uses a-z ISO basic Latin alphabet and ⟨é⟩, and their capitals. Names of the letters respect current usage, while ⟨é⟩ can be called /é/. Also allows use of acute accent and umlaut on vowels for expanded use.

This system can be further modified for specific needs. For instance, ⟨ë⟩ can be used instead of ⟨é⟩ with Contemporary Korean Romanization and ⟨S⟩ can be mapped to ⟨ㅆ⟩ for contemporary Korean for an input method. Another example is that ⟨h⟩, ⟨x⟩ can be used for ⟨ㅎ⟩, ⟨ㆆ⟩ with Middle Korean Romanization.

Try typing with it in a LaTeX environment `here <https://www.overleaf.com/read/jrtwwbczwhdw>`_. `Compare it <https://docs.google.com/spreadsheets/d/1Ntv-ASaK876rShBZtuuG2Jsas1JN6aofEWTalvjWjuI/edit#gid=0>`_ with other historic Romanizations. 

..
   |Open in Gitpod|

.. raw:: html

   <!--
   [Full specifications here](https://github.com/coughingmouse/Korean-Romanization/blob/main/main.md).
   Why this came to be is written [here](https://github.com/coughingmouse/Korean-Romanization/blob/main/why.md). 
   In making this system, a [proto](https://github.com/coughingmouse/Korean-Romanization/blob/main/proto.md) version was [modified](https://github.com/coughingmouse/Korean-Romanization/blob/main/ASCII.md) to fit with ASCII, then was further modified and extended to fit every practical need coherently.
   Thanks to a particular Joris Bohnson for suggesting to include diacritics and digraphs.
   Thanks to DepressionDokkebi for suggesting the use of ⟨ë⟩ for Contemporary Korean Romanization.
   -->

*******************
Contemporary Korean
*******************

Note that the letters (phonemes) "v", "z", and the silent vowel "o" are
absolutely neutralized.

Consonants
==========

==== == ==== == == 
ㅁ   ㅂ (ㅸ) ㅃ ㅍ
---- -- ---- -- -- 
m    b  v    bb p  
---- -- ---- -- -- 
ㄴ   ㄷ      ㄸ ㅌ
---- ------- -- -- 
n    d       dd t
---- ------- -- --  
(ᄛ) ㅈ      ㅉ ㅊ
---- ------- -- -- 
r    j       jj c
---- ------- -- -- 
ㄹ   ㅅ (ㅿ) ㅆ ㅎ
---- -- ---- -- -- 
l    s  z    ss x
---- -- ---- -- -- 
ㆁ   ㄱ      ㄲ ㅋ
---- ------- -- -- 
ng   q       qq k
==== ======= == ==

Vowels
======

===== == == == == == == ===== ===== ===== =====
ㅏ    ㅐ       ㅔ       ㅣ    ㅡ    ㅓ    (ㆍ)
----- -------- -------- ----- ----- ----- -----
`a`_  é(`eh`_)          `i`_  `u`_  `e`_  o
----- ----------------- ----- ----- ----- -----
ㅘ    ㅚ    ㅙ    ㅞ    ㅟ    ㅜ    ㅝ    ㅗ
----- ----- ----- ----- ----- ----- ----- -----
`wa`_ wé(`weh`_)        `wi`_ `wu`_ `we`_ `wo`_
----- ----------------- ----- ----- ----- -----
ㅑ    ㅒ       ㅖ       ㅢ    ㅠ    ㅕ    ㅛ
----- -------- -------- ----- ----- ----- -----
`ya`_ yé(`yeh`_)        `yi`_ `yu`_ `ye`_ `yo`_
===== ================= ===== ===== ===== =====

.. _a: /en/latest/_static/korean-audio/speech_a.mp3
.. _eh: /en/latest/_static/korean-audio/speech_eh.mp3
.. _i: /en/latest/_static/korean-audio/speech_i.mp3
.. _u: /en/latest/_static/korean-audio/speech_u.mp3
.. _e: /en/latest/_static/korean-audio/speech_e.mp3

.. _wa: /en/latest/_static/korean-audio/speech_wa.mp3
.. _weh: /en/latest/_static/korean-audio/speech_weh.mp3
.. _wi: /en/latest/_static/korean-audio/speech_wi.mp3
.. _wu: /en/latest/_static/korean-audio/speech_wu.mp3
.. _we: /en/latest/_static/korean-audio/speech_we.mp3
.. _wo: /en/latest/_static/korean-audio/speech_wo.mp3

.. _ya: /en/latest/_static/korean-audio/speech_ya.mp3
.. _yeh: /en/latest/_static/korean-audio/speech_yeh.mp3
.. _yi: /en/latest/_static/korean-audio/speech_yi.mp3
.. _yu: /en/latest/_static/korean-audio/speech_yu.mp3
.. _ye: /en/latest/_static/korean-audio/speech_ye.mp3
.. _yo: /en/latest/_static/korean-audio/speech_yo.mp3

..
   .. raw:: html
      <!--
      <audio controls="controls">
            <source src="/en/latest/_static/korean-audio/speech_a.mp3" type="audio/mpeg">
            Your browser does not support the <code>audio</code> element. 
      </audio>
      -->

Orthography
===========

You can follow Republic Korean spelling rules but you should apply the
following.

1. Write 'ㅎ' 불규칙 활용 explicitly with ⟨xo⟩.
2. Write 'ㅂ' 불규칙 활용 explicitly with ⟨v⟩.
3. Write 'ㅅ' 불규칙 활용 explicitly with ⟨z⟩.
4. Write '르' 불규칙 활용 explicitly with ⟨ll⟩ and '러' 불규칙 활용 with
   ⟨lll⟩.
5. Write 'ㄷ' 불규칙 활용 explicitly with ⟨r⟩.
6. Write '여' 불규칙 활용 without modifying the stem with ⟨ye⟩/⟨h⟩.

It is also recommended to apply the following changes.

1. Write 사이비읍 explicitly as ⟨bs⟩ or ⟨bd⟩.
2. Write 사이히읗 explicitly as ⟨x⟩.
3. Write 사이시읏 explicitly as ⟨s⟩, ⟨n⟩, or ⟨nn⟩.
4. Write the lexeme of the lemma "puji" as ⟨pu-⟩ instead of ⟨pwu-⟩.
5. Write the lemma previously written as "xiux" as "xius".
6. Write as "-ㄹ고, -ㄹ가, -ㄹ소냐"; not "-ㄹ꼬, -ㄹ까, -ㄹ쏘냐".

It is discouraged to do so, but you can transliterate as ㅐ ah, ㅚ woh, ㅙ wah, ㅒ yah.

*************
Middle Korean
*************

Consonants
==========

== == == ==
ㄱ q  ㆁ ng
-- -- -- --
ㅋ k  ㆆ H
ㄴ n  ㅎ x
ㄷ d  ㅈ j
ㅌ t  ㅊ c
ㄹ l  ㅁ m
ᄛ r  ㅸ v
ㅅ s  ㅂ b
ㅿ z  ㅍ p
== == == ==

Vowels
======

=== === === === ====
ㅏ  ㅓ  ㅡ  ㆍ  
--- --- --- --- ----
a   e   u   o       
ㅘ  ㅝ  ㅜ  ㅗ
wa  we  wu  wo  
ㅑ  ㅕ  ㅠ  ㅛ  
ya  ye  yu  yo
ㅐ  ㅔ  ㅢ  ㆎ
ai  ei  ui  oi  
ㅙ  ㅞ  ㅟ  ㅚ
wai wei wui woi 
ㅒ  ㅖ  ㆌ  ㆉ  ㅣ
yai yei yui yoi (y)i
=== === === === ====

*************
Hangul
*************

Consonants
==========

== == == ==
ㆁ ng ㅱ w
-- -- -- --
ㄱ q  ㅸ v
ㄲ qq ㅹ vv
ㅋ k  ㆄ f
ㄴ n  ᄛ r
ㄷ d  ㅈ j
ㄸ dd ㅉ jj
ㅌ t  ㅊ c
ㄹ l  ㅁ m
ㅅ s  ㅂ b
ㅆ ss ㅃ bb
ㅿ z  ㅍ p
ㆆ H  ㅎ x
ᄾ sh ᄼ cs
ᄿ zh ᄽ cz
ᅐ qh ᅎ cq
ᅑ jh ᅏ cj
ᅕ ch ᅔ cc
== == == ==

Vowels
==========================

=== === === === ==
ㅏ  ㅓ  ㅡ  ㆍ  ㅣ
--- --- --- --- --
a   e   u   o   i
ㅘ  ㅝ  ㅜ  ㅗ
óa  úe  ú   ó   
ㅑ  ㅕ  ㅠ  ㅛ
ya  ye  yú  yó
ㅐ  ㅔ  ㅢ  ㆎ  
ai  ei  ui  oi
ㅙ  ㅞ  ㅟ  ㅚ
óai úei úi  ói
ㅒ  ㅖ  ㆌ  ㆉ
yai yei yúi yói
=== === === === ==

 - Unspecified vowel clusters are transliterated as the combination of
   each traditionally minimal vowels.

 - ⟨i⟩ is used if it is or is a part of a nucleus. ⟨y⟩ is used if it is
   the first letter of a vowel cluster. Else, ⟨i⟩ is used: when it's the
   last letter of a vowel cluster.

 - In case only ASCII latin letters can be used, accent markers can be 
   replaced by a following h.
   
 - Use hyphens to distinguish syllbles if necessary.

Comparison
==========

The Romanization for Middle Korean introduced here covers more letter
than Yale Romanization and differs in a few ways. Here are some very
brief examples.

+------+----------+------+------------------------------------------+
| Text | Here     | Yale | Why                                      |
+======+==========+======+==========================================+
| ㄱ   | q        | k    | To get rid of confusion with ⟨ng⟩ and    |
|      |          |      | for compatibility with Chinese           |
+------+----------+------+------------------------------------------+
| ㅋ   | k        | kh   | Follows change of ⟨ㄱ⟩, Compression      |
+------+----------+------+------------------------------------------+
| ㆆ   | H        | q    | ⟨q⟩ is reserved                          |
+------+----------+------+------------------------------------------+
| ㅎ   | x        | h    | ⟨h⟩ is reserved                          |
+------+----------+------+------------------------------------------+
| ㅗ   | ó(wo)    | (w)o | To differenciate transliteration and     |
|      |          |      | transcription                            |
+------+----------+------+------------------------------------------+
| ㅜ   | ú(wu)    | (w)u | "                                        |
+------+----------+------+------------------------------------------+
| ㅐ   | ai       | ay   | To write semivowel ⟨ㅣ⟩ with a vowel     |
|      |          |      | character                                |
+------+----------+------+------------------------------------------+
| ㅔ   | ei       | ey   | "                                        |
+------+----------+------+------------------------------------------+
| ㅊ   | c        | ch   | To compress                              |
+------+----------+------+------------------------------------------+
| ㅈ   | j        | c    | For compatibility with Chinese and       |
|      |          |      | Middle Korean                            |
+------+----------+------+------------------------------------------+
| (ㅅ) | d/n/nn/r | q    | To define explicitly and phonemically    |
+------+----------+------+------------------------------------------+
| ㅸ   | v        | W    | To define explicitly and to follow       |
|      |          |      | historic pronunciation                   |
+------+----------+------+------------------------------------------+

*************
Miscellaneous
*************

Signs
=====

====== ======= =======
Hangul Accent  Tone
====== ======= =======
ㅇ     G       G
\〮     ◌\'     ◌\'
\〯     \'◌     ◌\"
Hangul Transc. Transl.
◌ː     \'◌     ◌:
====== ======= =======

-  For combinational letter clusters, just write them out by each
   compositing letter.

Early Modern Korean
===================

== == == == ==
ㅐ ㅔ ㅟ ㅚ ㅢ
-- -- -- -- --
ä  ë  ü  ö  ï
ㅒ ㅖ  
yä yë       
ㅞ ㅙ
wä wë 
== == == == ==

You may postpend ⟨E⟩ instead of the umlaut.

Phonetic Representation of Modern Korean
========================================

You can use combinations of the graphemes introduced above 
to represent the phonemes of dialectic Korean and the phones.

=== ====
Examples
========
ㆊ  ᄄᆑ
üe  ddüe
=== ====

.. |Open in Gitpod| image:: https://gitpod.io/button/open-in-gitpod.svg
   :target: https://gitpod.io/#github.com/coughingmouse/Korean-Romanization
