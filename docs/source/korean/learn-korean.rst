############
Learn Korean
############

  | There should be one-- and preferably only one --obvious way to do it.
  | Although that way may not be obvious at first unless you're Dutch.
  | - The Zen of Python

* Keywords to search with (+ Korean)

  * Content and language integrated learning (CLIL)
  * Content-based instruction (CBI)
  * Comprehensible Input

* Tools

  * Learn the sounds of Korean

  * Practice Spoken Korean in some `korean speaking environment <https://vrchat.com/home/world/wrld_c215101b-8f13-408c-8bc6-1687cf87762f>`_ on the biggest metaverse, VRChat.

  * Practice typing on VRChat or on `Discord <https://discord.com/login>`_ with your friends.

  * Use `Anki <https://apps.ankiweb.net/>`_ to memorize letters

  * Download and use `Daum dictionary <https://dic.daum.net/>`_ for English speakers and `Naver dictionary <https://dict.naver.com/>`_ for the rest

  * Use THE `grammar checker <http://speller.cs.pusan.ac.kr/>`_

* `Test your level <https://nuri.iksi.or.kr/front/page/participation/onlineLevelTest/main.do>`_

* Learn Hangul, the Korean Alphabet

  * Learn How Hangul works

  * Memorize the letters

    * Try `my deck <https://ankiweb.net/shared/info/63180203>`_.

* Get some textbook

  * Integrated Korean (You can use `Anna's Archive <https://annas-archive.org/>`_ to your advantage) (Recommended)

  * `Sejong Korean <https://mawusu.readthedocs.io/en/latest/korean/korean-textbooks.html>`_ (Open Source)

* Watch `Prof Yoon’s lecture <https://www.youtube.com/@ProfYoonsKoreanLanguageClass/playlists>`_

  * `Integrated Korean Videos <https://www.youtube.com/watch?v=XnXu1PSeI_c&list=PL6D-YTosvGYoi-eplvljZksq2I2PAKdiC>`_

  * `Sejong Korean Videos <https://www.youtube.com/watch?v=QV7QvtMREHM&list=PL6D-YTosvGYq4BSGSUZom6etXcXcAu2Ym&pp=iAQB>`_


* How To Study Korean

* Memrise 

* Flashcards on `Anki <https://apps.ankiweb.net/>`_

  * Grammar

    * `Korean Grammar Sentences by Evita <https://ankiweb.net/shared/info/3614346923>`_

  * Sentences
 
    * `🦑 Korean Sentences (Beginner: 1 to 1000) <https://ankiweb.net/shared/info/380675474>`_

    * `🦑 Korean Sentences (Intermediate: 1001 to 3000) <https://ankiweb.net/shared/info/374470252>`_

    * `🦑 Korean Sentences (Advanced: 3001 to 7000) <https://ankiweb.net/shared/info/893484891>`_

  * Vocabulary from `Retro's Decks <https://retrolearnskorean.blogspot.com/2020/08/anki-decks-for-korean-learners.html>`_

    * A1
 
      * `TTMIK's First 500 Korean Words by Retro [pictures+audio] <https://ankiweb.net/shared/info/1551455917>`_
 
      * `Retro's Beginner Korean Grammar Sentences (333) <https://ankiweb.net/shared/info/1842919283>`_
 
    * A2
 
      * `Retro's Beginner Korean Vocabulary Sentences (1539) <https://drive.google.com/drive/folders/1FemoEaheHiJy2eEtTQXGU_bNj8yQipjo?usp=sharing>`_
 
    * B1
 
      * `Retro's Intermediate Korean Vocabulary Sentences (1526) <https://drive.google.com/drive/folders/1FemoEaheHiJy2eEtTQXGU_bNj8yQipjo?usp=sharing>`_
 
      * `Retro's Intermediate Korean Grammar Sentences (366) <https://drive.google.com/drive/folders/1FemoEaheHiJy2eEtTQXGU_bNj8yQipjo?usp=sharing>`_
 
* Listening Resources

  * B2-C1

    * `100 free audiobooks for Korean learners <https://audioclip.naver.com/channels/57/clips/158>`_ in the order at `the book website <http://commbooks.com/%EB%8F%84%EC%84%9C/%EC%99%B8%EA%B5%AD%EC%9D%B8%EC%9D%84-%EC%9C%84%ED%95%9C-%ED%95%9C%EA%B5%AD%EC%96%B4-%EC%9D%BD%EA%B8%B0-%EC%84%B8%ED%8A%B8-%EC%A0%84100%EA%B6%8C/>`_. I understand that the first 50 books are for B2, the next 30 books B2 to C1, and the last 20 books to be C1 level. But I didn't go through them, so take this with a grain of salt.

* `italki <https://www.italki.com/>`_ for paid tutors

* `HelloTalk <https://www.hellotalk.com/>`_ for language exchange

  ..
      .. raw:: html

        <audio controls="controls">
              <source src="/en/latest/_static/korean-audio/speech_rights.mp3" type="audio/mpeg">
              Your browser does not support the <code>audio</code> element. 
        </audio>

      | |image1| |image1|   |image1| |image1| |image1| ...
      | 모든 인간은 태어날 때부터 자유로우며 그 존엄과 권리에 있어 동등하다. 
      | 인간은 천부적으로 이성과 양심을 부여받았으며 서로 형제애의 정신으로 행동하여야 한다.

      .. |image1| image:: https://www.svgrepo.com/show/238564/diagonal-arrow.svg
        :width: 12
        :alt: Image of an arrow from top-left to bottom-right

      .. raw:: html

        <audio controls="controls">
              <source src="/en/latest/_static/korean-audio/speech_all.mp3" type="audio/mpeg">
              Your browser does not support the <code>audio</code> element. 
        </audio>
      
      | "웬 초콜릿? 제가 원했던 건 뻥튀기 쬐끔과 의류예요." "얘야, 왜 또 불평?"

    | Hangul is an alphabet, meaning it is a system that consists of letters, graphemes that correspond to phonemes.
    | Hangul is written by characters, and is read left-to-right and top-to-bottom. 
    | Each character, which fits in a square like Chinese or Japanese characters, represents a syllable. 
    | Each character consists of letters, like how each syllable consists of phonemes. 

    | The first letter of a character is a consonant or a null consonant ⟨ㅇ⟩. 
    | The letters are written on the bottom or the right of the previously written letters.
    | When writing vowels, the ends of the longer stroke of the letter are not blocked by the initial consonant block when they are written.
    | Consonants that come after the vowels are written below the vowels.

    | Phonemes corresponding to some letters sound quite different depending on its relative position in a word. 
    | Four of them are absolutely neutralized, meaning they do not have a distinctive pronunciation.
    | One of them is not pronounced at all.
  
      .. raw:: html

        <audio controls="controls">
              <source src="/en/latest/_static/korean-audio/speech_consonants.mp3" type="audio/mpeg">
              Your browser does not support the <code>audio</code> element. 
        </audio>

      | 가나다라 마바사, 아자차카 타파하!
      
      .. raw:: html

        <audio controls="controls">
              <source src="/en/latest/_static/korean-audio/speech_vowels.mp3" type="audio/mpeg">
              Your browser does not support the <code>audio</code> element. 
        </audio>
        
      | 아와야 에외예 이위의의 으우유 어워여 오~요!

      | Like all characters, the shapes of the letters will differ depending on the font or the handwriting.

      Teaching order
      ^^^^^^^^^^^^^^

      #. 아, 에, 이
      #. 마, 나, 라
      #. 애, 으, 어
      #. 바, 다, 사
      #. 자, 받침, 앙
      #. 와, 왜/웨/외, 위
      #. 우, 워, 오
      #. 야, 예/얘, 의
      #. 유, 여, 요
      #. 파, 타, 카
      #. 차, 싸, 짜
      #. 빠, 따, 까

    * Letters
..
  ===== ==== ==== == ==== 
  M     B    V    BB P 
  ===== ==== ==== == ====
  ㅁ    ㅂ    b/w ㅃ ㅍ
  ----- ---- ---- -- ---- 
  `m`_  `b`_ v    bb `p`_
  ----- ---- ---- -- ---- 
  ㄴ    ㄷ        ㄸ ㅌ
  ----- --------- -- ---- 
  `n`_  `d`_      dd `t`_
  ----- --------- -- ----  
  d/l   ㅈ        ㅉ ㅊ
  ----- --------- -- ---- 
  r     `j`_      jj `c`_
  ----- --------- -- ---- 
  ㄹ    ㅅ    s/∅ ㅆ ㅎ
  ----- ---- ---- -- ---- 
  `l`_  `s`_ z    ss `x`_
  ----- ---- ---- -- ---- 
  ㆁ    ㄱ        ㄲ ㅋ
  ----- --------- -- ---- 
  `ng`_ `q`_      qq `k`_
  ===== ========= == ====

  .. _m: /en/latest/_static/korean-audio/speech_m.mp3
  .. _b: /en/latest/_static/korean-audio/speech_b.mp3
  .. _p: /en/latest/_static/korean-audio/speech_p.mp3

  .. _n: /en/latest/_static/korean-audio/speech_n.mp3
  .. _d: /en/latest/_static/korean-audio/speech_d.mp3
  .. _t: /en/latest/_static/korean-audio/speech_t.mp3

  .. _j: /en/latest/_static/korean-audio/speech_j.mp3
  .. _c: /en/latest/_static/korean-audio/speech_c.mp3

  .. _l: /en/latest/_static/korean-audio/speech_l.mp3
  .. _s: /en/latest/_static/korean-audio/speech_s.mp3
  .. _x: /en/latest/_static/korean-audio/speech_x.mp3

  .. _ng: /en/latest/_static/korean-audio/speech_ng.mp3
  .. _q: /en/latest/_static/korean-audio/speech_q.mp3
  .. _k: /en/latest/_static/korean-audio/speech_k.mp3
..
  +----------+----------------+------------+
  | A        | WA             | YA         |
  +==========+================+============+
  | ㅏ       | ㅘ             | ㅑ         |     
  +----------+----------------+------------+
  | `a`_     | `wa`_          | `ya`_      |
  +----------+----------------+------------+
  | ㅐ,      | ㅚ,            | ㅒ,        |
  | ㅔ       | ㅙ,            | ㅖ         |
  |          | ㅞ             |            |
  +----------+----------------+------------+
  | ah,      | woh,           | yah,       |
  | `eh`_    | wah,           | `yeh`_     |
  |          | `weh`_         |            |
  +----------+----------------+------------+
  | ㅣ       | ㅟ             | ㅢ         |
  +----------+----------------+------------+
  | `i`_     | `wi`_          | `yi`_      |
  +----------+----------------+------------+
  | ㅡ       | ㅜ             | ㅠ         |
  +----------+----------------+------------+
  | `u`_     | `wu`_          | `yu`_      |
  +----------+----------------+------------+
  | ㅓ       | ㅝ             | ㅕ         |
  +----------+----------------+------------+
  | `e`_     | `we`_          | `ye`_      |
  +----------+----------------+------------+
  | ∅        | ㅗ             | ㅛ         |
  +----------+----------------+------------+
  | o        | `wo`_          | `yo`_      |      
  +----------+----------------+------------+

  .. _a: /en/latest/_static/korean-audio/speech_a.mp3
  .. _eh: /en/latest/_static/korean-audio/speech_eh.mp3
  .. _i: /en/latest/_static/korean-audio/speech_i.mp3
  .. _u: /en/latest/_static/korean-audio/speech_u.mp3
  .. _e: /en/latest/_static/korean-audio/speech_e.mp3

  .. _wa: /en/latest/_static/korean-audio/speech_wa.mp3
  .. _weh: /en/latest/_static/korean-audio/speech_weh.mp3
  .. _wi: /en/latest/_static/korean-audio/speech_wi.mp3
  .. _wu: /en/latest/_static/korean-audio/speech_wu.mp3
  .. _we: /en/latest/_static/korean-audio/speech_we.mp3
  .. _wo: /en/latest/_static/korean-audio/speech_wo.mp3

  .. _ya: /en/latest/_static/korean-audio/speech_ya.mp3
  .. _yeh: /en/latest/_static/korean-audio/speech_yeh.mp3
  .. _yi: /en/latest/_static/korean-audio/speech_yi.mp3
  .. _yu: /en/latest/_static/korean-audio/speech_yu.mp3
  .. _ye: /en/latest/_static/korean-audio/speech_ye.mp3
  .. _yo: /en/latest/_static/korean-audio/speech_yo.mp3

..
  * Learn how to read
  
  | Click on the characters on the tables above to listen to the Korean pronunciations of Xanqul.
  | The following format is used to illustrate the pronunciations of consonants.

    | /ba bbei bibubbe/
    | /bwa bbwei bwibwubbebwo/
    | /bya bbyei byibyubbyebyo/

  | Letters v, r, z, o are absolutely netralized: that is they are pronounced like other phonemes or not pronounced.
  | They are pronounced as below.

..
  .. math::
    \begin{eqnarray}
    ⫽v⫽
    \rightarrow
    \begin{cases}
    /w/,&\text{ if vowel follows }\\
    /b/ &\text{ otherwise }
    \end{cases}
    \end{eqnarray}

  .. math::
    \begin{eqnarray}
    ⫽r⫽
    \rightarrow
    \begin{cases}
    /l/,&\text{ if vowel follows }\\
    /d/ &\text{ otherwise }
    \end{cases}
    \end{eqnarray}

  .. math::
    \begin{eqnarray}
    ⫽z⫽
    \rightarrow
    \begin{cases}
    /\varnothing/,&\text{ if vowel follows }\\
    /s/ &\text{ otherwise }
    \end{cases}
    \end{eqnarray}

  .. math::
    \begin{eqnarray}
    ⫽o⫽
    \rightarrow
    /\varnothing/
    \end{eqnarray}
