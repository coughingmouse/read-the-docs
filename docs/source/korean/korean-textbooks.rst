##############################
Korean Textbooks / 한국어 교재
##############################

| Hello, dear Korean learners and teachers!
| 한국어를 배우거나 가르치려는 여러분을 환영합니다.

| Here are some **Korean textbooks** by *Sejong Institute* that you can download.
| 세종학당에서 배포한 한국어 교재를 제공드립니다.

| The license for the books only say I need to attribute the original creator, use it non-commercially, and to not modify the content. The license providing authority says using image within a part of a video is fine, so converting it to a PDF should be fine.
| 라이센스에 따르면 저작권자를 명시하고 수익을 창출하지 않으며 형식을 바꾸면 안 된다고 합니다. 라이센스를 만든 측에 따르면 이미지를 영상에 추가하는 정도는 괜찮다고 하니 PDF로의 변환은 괜찮은 것 같습니다.

   * Sejong Korean (세종한국어) 1A-4B
   * Sejong Korean Exercise (세종한국어 익힘책) 1A-4B
   * Sejong Korean Vocabulary/Expressions and Grammar (세종한국어 어휘·표현과 문법) 1A-4B
   * Sejong Korean Additional Activities (세종한국어 더하기 활동) 1A-4B

   * King Sejong Institute Korean Introduction (세종학당 한국어 입문) (Korean, English, other languages)
   * King Sejong Institute Practical Korean (세종학당 실용 한국어) 1-2 (English, other languages)
   * King Sejong Institute Practical Korean (세종학당 실용 한국어) 3-4

   * 세종학당 실용 한국어 1~4 교원용 지침서
   * 외국인을 위한 한국어 문법1~2
   * 세종통번역
   * 세종통번역 교원용 지침서

| You can download directly from `Mega <https://mega.nz/folder/jNQwRCDb#rnjWNqz-jpFlK7utkkxotw>`_ or `Baidu <https://pan.baidu.com/s/149O09ebsPL-1NnOaxKMxKA?pwd=8c8r>`_.
| Alternatively, I was trying to setup `Torrent <https://gitlab.com/coughingmouse/mawusu/-/raw/main/Korean.torrent?inline=false>`_ or a `magnet address <magnet:?xt=urn:btih:b9142bcda0488108930b73b7f92cee6477872e98&dn=Korean&tr=udp%3A%2F%2F93.158.213.92%3A1337%2Fannounce&tr=udp%3A%2F%2F23.137.251.45%3A6969%2Fannounce&tr=udp%3A%2F%2F45.154.253.10%3A6969%2Fannounce&tr=udp%3A%2F%2F151.80.120.112%3A2810%2Fannounce&tr=udp%3A%2F%2F185.44.82.25%3A1337%2Fannounce&tr=udp%3A%2F%2F185.181.60.155%3A80%2Fannounce&tr=udp%3A%2F%2F91.216.110.53%3A451%2Fannounce&tr=udp%3A%2F%2F107.189.11.58%3A6969%2Fannounce&tr=udp%3A%2F%2F178.170.48.154%3A1337%2Fannounce&tr=udp%3A%2F%2F156.234.201.18%3A80%2Fannounce&tr=udp%3A%2F%2F176.56.3.118%3A6969%2Fannounce&tr=udp%3A%2F%2F209.141.59.16%3A6969%2Fannounce&tr=udp%3A%2F%2F185.134.22.3%3A6969%2Fannounce&tr=udp%3A%2F%2F109.201.134.183%3A80%2Fannounce&tr=udp%3A%2F%2F185.230.4.150%3A1337%2Fannounce>`_ but I do not have my laptop on very often, so this is often unavilable.
| Other books and PDFs for 세종한국어 교사용 지도서 1~4 can be found on the `official Nuri Sejong Institute website <https://nuri.iksi.or.kr>`_.
| You can manually download PDFs from source images using `this <https://github.com/coughingmouse/Download-Korean-Textbooks/>`_.

| 외국인을 위한 한국어 문법 1~2 are copyrighted by 국립국어원.
| The rest are copyrighted by 세종학당재단.
| Here I clarify that I am not an affiliate of either of these institutes.

.. autosummary::
   :toctree: generated
