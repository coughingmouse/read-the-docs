================
Sounds of Korean
================

There is `liaison <https://en.m.wikipedia.org/wiki/Liaison_(French)>`_ in Korean.

Sounds with regard to morphophonemes
------------------------------------

I recommend finding a quiet place where you can practice pronuncations without getting heard by people you don't want to disturb.

+----------+----------------+------------+
| ㅏ       | ㅘ             | ㅑ         |
+----------+----------------+------------+
| `a`_     | `wa`_          | `ya`_      |
+----------+----------------+------------+
| ㅐ,      | ㅚ,            | ㅒ,        |
| ㅔ       | ㅙ,            | ㅖ         |
|          | ㅞ             |            |
+----------+----------------+------------+
| `eh`_    | `weh`_         | `yeh`_     |
+----------+----------------+------------+
| ㅣ       | ㅟ             | ㅢ         |
+----------+----------------+------------+
| `i`_     | `wi`_          | `yi`_      |
+----------+----------------+------------+
| ㅡ       | ㅜ             | ㅠ         |
+----------+----------------+------------+
| `u`_     | `wu`_          | `yu`_      |
+----------+----------------+------------+
| ㅓ       | ㅝ             | ㅕ         |
+----------+----------------+------------+
| `e`_     | `we`_          | `ye`_      |
+----------+----------------+------------+
| ∅        | ㅗ             | ㅛ         |
+----------+----------------+------------+
| o        | `wo`_          | `yo`_      |
+----------+----------------+------------+

.. _a: /en/latest/_static/korean-audio/speech_a.mp3
.. _eh: /en/latest/_static/korean-audio/speech_eh.mp3
.. _i: /en/latest/_static/korean-audio/speech_i.mp3
.. _u: /en/latest/_static/korean-audio/speech_u.mp3
.. _e: /en/latest/_static/korean-audio/speech_e.mp3

.. _wa: /en/latest/_static/korean-audio/speech_wa.mp3
.. _weh: /en/latest/_static/korean-audio/speech_weh.mp3
.. _wi: /en/latest/_static/korean-audio/speech_wi.mp3
.. _wu: /en/latest/_static/korean-audio/speech_wu.mp3
.. _we: /en/latest/_static/korean-audio/speech_we.mp3
.. _wo: /en/latest/_static/korean-audio/speech_wo.mp3

.. _ya: /en/latest/_static/korean-audio/speech_ya.mp3
.. _yeh: /en/latest/_static/korean-audio/speech_yeh.mp3
.. _yi: /en/latest/_static/korean-audio/speech_yi.mp3
.. _yu: /en/latest/_static/korean-audio/speech_yu.mp3
.. _ye: /en/latest/_static/korean-audio/speech_ye.mp3
.. _yo: /en/latest/_static/korean-audio/speech_yo.mp3

* There's also the ㆊ /üe/ sound. Ask some Korean how they would pronounce the word for "jump" ᄄᆑ!

⟨h⟩ represents a higher `register <https://en.wikipedia.org/wiki/Register_(phonology)>`_. 

1. After ⟨p,t,c,k⟩ it adds a high pitch. That is, ⟨ph⟩ is pronounced with a higher pitch while ⟨p⟩ is pronounced with a low pitch. Otherwise, the pronunciation is not distinguishable.
1. After ⟨s,x⟩ it `palatalizes <https://en.wikipedia.org/wiki/Palatalization_(phonetics)>`_ the consonant. That is, the phoneme's prounounced consonant changes.

+--------+-----+------+--+--------+
| ㅁ     | ㅂ  | b/w  |ㅃ|ㅍ      |
| `m`_   | `b`_| v    |bb|`p`_    |
+--------+-----+------+--+--------+
| m      | p/b | v    |pp| ph     |
+--------+-----+------+--+--------+
| ㄴ     | ㄷ         |ㄸ|ㅌ      |
| `n`_   | `d`_       |dd|`t`_    |
+--------+------------+--+--------+
| (d/)n  | t/d        |tt| th     |
+--------+-----+------+--+--------+
| d/l    | ㅈ         |ㅉ|ㅊ      |
| r      | `j`_       |jj|`c`_    |
+--------+-----+------+--+--------+
| d;r    | c/j        |cc| ch     |
+--------+-----+------+--+--------+
| ㄹ     | ㅅ  | s/∅  |ㅆ|ㅎ      |
| `l`_   | `s`_| z    |ss|`x`_    |
+--------+-----+------+--+--------+
|l/r,ll/l| s.sh| z    |ss|h.xh.x/x|
+--------+-----+------+--+--------+
| ㆁ     | ㄱ         |ㄲ|ㅋ      |
| `ng`_  | `q`_       |qq|`k`_    |
+--------+-----+------+--+--------+
| ng     | k/q        |kk| kh     |
+--------+-----+------+--+--------+
.. _m: /en/latest/_static/korean-audio/speech_m.mp3
.. _b: /en/latest/_static/korean-audio/speech_b.mp3
.. _p: /en/latest/_static/korean-audio/speech_p.mp3

.. _n: /en/latest/_static/korean-audio/speech_n.mp3
.. _d: /en/latest/_static/korean-audio/speech_d.mp3
.. _t: /en/latest/_static/korean-audio/speech_t.mp3

.. _j: /en/latest/_static/korean-audio/speech_j.mp3
.. _c: /en/latest/_static/korean-audio/speech_c.mp3

.. _l: /en/latest/_static/korean-audio/speech_l.mp3
.. _s: /en/latest/_static/korean-audio/speech_s.mp3
.. _x: /en/latest/_static/korean-audio/speech_x.mp3

.. _ng: /en/latest/_static/korean-audio/speech_ng.mp3
.. _q: /en/latest/_static/korean-audio/speech_q.mp3
.. _k: /en/latest/_static/korean-audio/speech_k.mp3

* Note how the sounds of consonants differ when in the initial, middle, and final position of a word, and in front of certain vowels.

  * b/p, d/t, q/k, j/c are distinguishable in the word-initial position because ⟨b⟩ is pronounced with a low pitch and ⟨p⟩ is pronounced with a high pitch.

  * b/p, d/t, q/k, j/c are distinguishable in the word-middle position because ⟨b⟩ is voiced and ⟨p⟩ is voiceless, like in English and Japanese.

  * b/bb/p, d/dd/t/j/jj/c/s/ss, q/qq/k are not distinguishable in the word-final position.

* ⟨l⟩ may be pronounced in two ways depending on the position.

  * Word-initial ⟨l⟩ is pronounced as a light ⟨l⟩, like the word-initial ⟨l⟩ in English. But it is okay to pronouce it as a flap, like the word-middle ⟨t⟩ in General American (e.g. better).

  * Word-middle ⟨ll⟩ before ⟨a⟩ or ⟨e⟩ is pronounced as ⟨lu⟩, before a vowel is pronounced as double light ls ⟨ll⟩, like the word-initial ⟨l⟩ in English. Else, word-middle ⟨l⟩ is prounced as a flap, like the word-middle ⟨t⟩ in General American (e.g. better).

    * Word-middle ⟨lll⟩ is pronounced as <lu> in front of a consonant and as ⟨lul⟩ in front of ⟨e⟩. Else, word-middle ⟨ll⟩ before a consonant is pronounced as <lu>. In these cases, it is written as pronounced instead.

  * Word-final ⟨l⟩ sounds like a light ⟨l⟩, like the word-initial ⟨l⟩ in English.

* s and ss sound different and are pronounced differently, but they are very similar. 

  * Put the back of your hand a palm away from your mouth. You shouldn't feel much air coming out of your mouth when you pronounce ss, like when you pronounce the English Esssssss.
  * On the contrary, you have to be able to feel the air coming out of your mouth when you pronounce the s sound.

* ⟨ㅇ⟩ in modern use is actually a combination of two older letters that look very similar. ⟨ㅇ⟩ is a placeholder that is only used word-initially. ⟨ㆁ⟩ is the /ng/ sound only appearing at word-finals. Now you use ⟨ㅇ⟩ for both.

Learn how to read
~~~~~~~~~~~~~~~~~

| Click on the characters on the tables above to listen to the Korean pronunciations of Hangul.
| The following format is used to illustrate the pronunciations of consonants.

| /ba bbei bibubbe/
| /bwa bbwei bwibwubbebwo/
| /bya bbyei byibyubbyebyo/

| Letters (morphophonemes) ⟨v⟩, ⟨z⟩, and ⟨o⟩ are absolutely netralized: that is, they are always pronounced like other phonemes or not pronounced.
| Letters ⟨l⟩ and ⟨r⟩ are pronounced in a complex pattern.
| They are written in Korean how they are pronounced, on the contrary to how Korean is written usually (with morphophonemes).
| They are pronounced as below.


.. math::
  \begin{eqnarray}
  ⫽v⫽
  \rightarrow
  \begin{cases}
  /w/,&\text{ if vowel follows }\\
  /b/ &\text{ otherwise }
  \end{cases}
  \end{eqnarray}

.. math::
  \begin{eqnarray}
  ⫽r⫽
  \rightarrow
  \begin{cases}
  /r/,&\text{ if vowel follows }\\
  /d/ &\text{ otherwise }
  \end{cases}
  \end{eqnarray}
  
.. math::
  \begin{eqnarray}
  ⫽l⫽
  \rightarrow
  \begin{cases}
  ⫽ll⫽,&\text{ if ⟨l⟩ follows }\\
  /r/,&\text{ else if vowel follows }\\
  /l/ &\text{ otherwise }
  \end{cases}
  \end{eqnarray}
  
.. math::
  \begin{eqnarray}
  ⫽ll⫽
  \rightarrow
  \begin{cases}
  ⫽lll⫽,&\text{ if ⟨l⟩ follows }\\
  ⫽lu⫽,&\text{ else if ⟨a⟩ or ⟨e⟩ follows }\\
  /ll/ &\text{ otherwise }
  \end{cases}
  \end{eqnarray}

.. math::
  \begin{eqnarray}
  ⫽lll⫽
  \rightarrow
  \begin{cases}
  ⫽lul⫽,&\text{ if ⟨e⟩ follows }\\
  ⫽lu⫽ &\text{ otherwise }
  \end{cases}
  \end{eqnarray}

.. math::
  \begin{eqnarray}
  ⫽z⫽
  \rightarrow
  \begin{cases}
  /\varnothing/,&\text{ if vowel follows }\\
  /s/ &\text{ otherwise }
  \end{cases}
  \end{eqnarray}

.. math::
  \begin{eqnarray}
  ⫽o⫽
  \rightarrow
  /\varnothing/
  \end{eqnarray}

Prosody
-------

#. Statements always end with a low pitch syllable and questions always end with a high pitch syallable. If a sentence is a syllable, the pitch of the monosyllable goes upward or downward if the pitch of the initial of the monosyllable (e.g. ka - low pitch) does not match the sentence type (e.g. question - high pitch).

#. The first syllable of a word has a pitch depending on the first letter. The second syllable has a high pitch. The pitch of the following syllables generally go down every syllable unless the word ends with a connecting morpheme, where the last syllable has a high pitch.

There are more rules.

Phonological rules
------------------

There are too many rules. You should always ask Koreans how to write in hangul as well as how to write in hangul *as pronounced* to learn this by practicing.