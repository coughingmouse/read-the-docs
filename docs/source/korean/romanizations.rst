=============
Romanizations
=============

.. csv-table:: Comparison of Romanizations
   :file: romanizations.csv
   :header-rows: 1
   :class: longtable
   :stub-columns: 2
   :widths: auto
