Just So You Know
================

As far as I know, the `AJATT Resources List <https://tatsumoto.neocities.org/blog/resources>`_ is the most comprehensive and high quality resources list when it comes to learning Japanese.

The Open Source Times New Roman alternative, Nimbus Roman is now completely Open Source! Relicensed under OFL, all the variants are now available `here <https://github.com/twardoch/urw-core35-fonts>`_.

For a usual adult, the width of their nail is around 0.01 m, the width of limb 0.1 m, and the length of leg 1 m. A transit bus is about 10 m long and a soccer field is around 100 m long.

Money sucks in the long term; buy the same amount of QQQ and SCHD.

Do NOT gamble or abuse drugs: gambling is an action, not a type of game; alcohol and cigarette are very typically abused drugs.

두유 드세요. 홈플러스에서는 950 mL 개당 1500원 정도의 가격으로 판매합니다.

시간을 셀 때 다음과 같이 세면 어느 정도 정확합니다.
십 가화만사성 구 가화만사성 팔 가화만사성 칠 가화만사성 육 가화만사성 오 가화만사성 사 가화만사성 삼 가화만사성 이 가화만사성 일 가화만사성 영!

Pointers are location variables. Let's call them locations.

Why don't we write 2.9979 * 10^8 as, say, 2E8B9979.

Let's use the word "turn" (τ, tr) for 6.283185... rather than reading it as tau. We still can use the word "cycle" to mean a turn of radian.

Let's also use a set of dimension-free natural units where

.. math::

   c &= 1 \\
   \hbar &= 1 \\
   G_0 &= 2 \cdot \tau \cdot G = 4 \cdot \pi \cdot G = 1 \\
   \epsilon_0 &= 1.

Don't dare say that Europe and Asia are continents.
Don't group them under the same category like the Orientalists did. 

Asia comprises of several different cultural spheres, while Europe is only a part of the Mediterranean. 
Geographically, they aren't even comparable in size nor in any single geographical concept (unless you use Orientalist concepts; believing in flat earth is more fruitful than that).

Westerners try to justify by suiting the meaning of "continent" into "Europe" by saying it's "any of several large geographical regions", to quote Wikipedia. That's disgusting. Stop.
Continent, in the context I'm using it as, is "one of great parts of land on the globe". Asia, albeit being arbitrary, is a great part of the Old World. Because Asia is arbitrarily too big, the rest of Eurasia, a.k.a. Europe, is NOT a great part of land. Subsahara and the Mediterranean are the other two "great parts of land" of the Old World. Europe is only a part of the Mediterranean, along with North Africa.

You may ask: isn't the Old World a discriminatory phrase? Not really; it isn't used in a discriminatory way, nor does it possess an ill ideology.

Let's use the dymaxion-like conformal projection map of the world.

.. image:: https://map-projections.net/img/jpg-w/dymaxion-like-conformal.jpg

Ansible is not idempotent; it is univalent.

| Your Actions invoke Reward, which you associate with a Signal upon which you Crave.  
| You may not like your actions. You may be a slave of the past, but the future is yours.  
| Sabotage Actions, Counter Reward, Arrange Signals, and Repeat to revoke the Crave.

..
   | Before you go, I would like to ask you something simple, as a symbol of sobriety. 
   | Virtualize, defamiliarize, tradeoff, disentangle, you who refuse to be slaves!
