Conveniences
============

.. toctree::
    :maxdepth: 1

    p-layout
    jsyk
    Right Talk <https://gitlab.com/coughingmouse/right-talk/-/raw/main/right-talk-cheat-sheet.pdf?inline=true>
